# Node express server api

The Brazil dataset Server

## Getting started

These instructions will get you a copy of the project up and
running on your local machine for development.

As you can see, you can run it on your own machine or you
can choose to run it on docker.

### Prerequisites

Check the correct version of Npm and Node.

```
Npm 6.9.0

Node 11.12.0
```

## Coding Style

This project follows the airbnb and prettier coding style.

- See airbnb in [Github](https://github.com/airbnb/javascript)
- See prettier in [Github](https://github.com/prettier/prettier)

Always use two spaces and single quotes.

## Dataset of information

- See the brasil.io dataset constantly updated at [Google Drive](https://drive.google.com/drive/folders/1tOGB1mJZcF5V1SUS-YlPJF0-zdhfN1yd)

## Authors

- **Alan Cunha** - _Initial work_

## License

This project is UNLICENSED
