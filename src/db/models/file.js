import mongoose from 'mongoose';

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  kind: {
    type: String,
    required: true,
    enum: ['COMPANY', 'CNAE', 'PARTNER'],
  },
  migrationDate: {
    type: Date,
    required: true,
    default: Date.now,
  },
  sourceDate: {
    type: Date,
    required: true,
  },
  isLastMigration: {
    type: Boolean,
    default: true,
  },
  url: {
    type: String,
  },
});

export const File = mongoose.model('file', schema);

export default File;
