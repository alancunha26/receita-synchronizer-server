import mongoose from 'mongoose';
import phoneSchema from '../schemas/phone';
import addressSchema from '../schemas/address';

const schema = new mongoose.Schema({
  // Identifiers
  cnpj: {
    type: String,
    required: true,
  },
  legalNature: {
    type: String,
  },
  primaryCnae: {
    type: String,
  },
  secondaryCnae: {
    type: String,
  },

  // ObjectIDs
  primaryCnaeID: {
    type: mongoose.SchemaTypes.ObjectId,
  },
  secondaryCnaeID: {
    type: mongoose.SchemaTypes.ObjectId,
  },
  fileID: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },

  // Info
  startActivityDate: {
    type: Date,
  },
  businessName: {
    type: String,
  },
  fantasyName: {
    type: String,
  },
  email: {
    type: String,
  },
  kind: {
    type: String,
    enum: ['SIMPLES', 'MEI', 'ME', 'EPP'],
  },

  // Cadastral situation
  cadastralSituation: {
    type: String,
  },
  cadastralSituationDate: {
    type: Date,
  },
  cadastralSituationReason: {
    type: String,
  },

  // Contact
  address: {
    type: addressSchema,
  },
  phones: {
    type: [phoneSchema],
  },
});

export const Company = mongoose.model('company', schema);

export default Company;
