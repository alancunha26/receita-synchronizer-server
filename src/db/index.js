import mongoose from 'mongoose';

mongoose.connect(process.env.DB_HOST, { useNewUrlParser: true });
mongoose.Promise = global.Promise;

export { User } from './models/user';
export { Cnae } from './models/cnae';
export { File } from './models/file';
export { Company } from './models/company';

export default mongoose;
