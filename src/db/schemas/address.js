import mongoose from 'mongoose';

const addressSchema = new mongoose.Schema({
  street: {
    type: String,
  },
  number: {
    type: String,
  },
  zipCode: {
    type: String,
  },
  city: {
    type: String,
  },
  neighborhood: {
    type: String,
  },
  state: {
    type: String,
  },
  coordinates: {
    type: [Number],
  },
});

export default addressSchema;
