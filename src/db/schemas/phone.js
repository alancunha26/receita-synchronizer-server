import mongoose from 'mongoose';

const phoneSchema = new mongoose.Schema({
  kind: {
    type: String,
    enum: ['CELL', 'RESIDENTIAL', 'UNKNOW'],
    default: 'UNKNOW',
  },
  number: {
    type: String,
  },
});

export default phoneSchema;
