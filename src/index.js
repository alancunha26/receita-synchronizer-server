import app from './api';

const { API_PORT, API_PATH } = process.env;

/**
|--------------------------------------------------
| App initialization
|--------------------------------------------------
*/
app.listen({ port: API_PORT }, () => {
  console.log(`🚀  REST Server ready at http://localhost:${API_PORT}${API_PATH}`);
});

export default app;
