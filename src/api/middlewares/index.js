import authMiddleware from './authMiddleware';
import rolesMiddleware from './rolesMiddleware';
import validationMiddleware from './validationMiddleware';

const middlewares = [authMiddleware, rolesMiddleware, validationMiddleware];

export default middlewares;
