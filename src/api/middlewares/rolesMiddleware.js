import _ from 'lodash';

const rolesMiddleware = ({ roles, isAuth }) => (req, res, next) => {
  if (isAuth && roles && req.user) {
    const userRole = req.user.role;

    if (_.isString(roles)) {
      if (roles === 'any') {
        return res.next();
      }

      return res.status(403).send({
        error: `User of role ${userRole} is different from ${roles}`,
      });
    }

    if (_.isNumber(roles)) {
      if (userRole === roles) {
        return res.next();
      }

      return res.status(403).send({
        error: `User of role ${userRole} is different from ${roles}`,
      });
    }

    if (_.isArray(roles)) {
      if (roles.includes(userRole)) {
        return next();
      }

      return res.status(403).send({
        error: `User of role ${userRole} is different from ${roles.join(',')}`,
      });
    }

    if (_.isPlainObject(roles)) {
      const keys = Object.keys(roles);

      if (keys.includes(userRole)) {
        return next();
      }

      return res.status(403).send({
        error: `User of role ${userRole} is different from ${keys.join(',')}`,
      });
    }

    return res.status(403).send({
      error: 'No permission',
    });
  }

  return next();
};

export default rolesMiddleware;
