import isNull from 'lodash/isNull';
import Joi from 'joi';

const validationMiddleware = ({ schema }) => (req, res, next) => {
  if (isNull(schema)) {
    next();
  } else {
    const errors = [];

    ['params', 'body', 'query'].forEach((key) => {
      if (schema[key]) {
        const result = Joi.validate(req[key], schema[key]);

        if (result.error) {
          errors[key] = result.error;
        }
      }
    });

    if (errors && errors.length > 0) {
      res.status(400).json({
        errors,
      });
    }
  }
};

export default validationMiddleware;
