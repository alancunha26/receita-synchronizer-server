const defaultHandler = (req, res) => {
  res.status(404).json({
    errors: 'Not implemented yet',
  });
};

export default defaultHandler;
