import _ from 'lodash';

const applyMiddlewares = (data, ...middlewares) => {
  const appliedMiddlewares = [];

  _.forEach(middlewares, (middleware) => {
    if (_.isFunction(middleware)) {
      const appliedMiddleware = middleware(data);

      if (_.isFunction(appliedMiddleware)) {
        appliedMiddlewares.push(appliedMiddleware);
      }
    }
  });

  return appliedMiddlewares;
};

export default applyMiddlewares;
