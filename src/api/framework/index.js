import _ from 'lodash';
import path from 'path';
import express from 'express';
import defaultHandler from './defaultHandler';
import applyMiddlewares from './applyMiddlewares';

const initialize = ({
  app, apiPath, modules, middlewares,
}) => {
  _.forEach(modules, (module) => {
    const router = express.Router();

    const moduleName = module.name;
    const moduleApi = module.router || module.api;

    _.forEach(moduleApi, (api) => {
      const apiResolver = _.isNil(api.resolver) ? defaultHandler : api.resolver;

      const { method, path: modulePath } = api;

      router[_.lowerCase(method)](
        // path
        modulePath,

        // router middlewares
        ...applyMiddlewares(api, ...middlewares),

        // main resolver
        apiResolver,
      );
    });

    app.use(path.join(apiPath, moduleName), router);
  });
};

export default {
  initialize,
  applyMiddlewares,
};
