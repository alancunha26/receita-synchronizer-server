import fs from 'fs';
import AWS from 'aws-sdk';

const config = process.env;

AWS.config.update({
  region: config.AWS_REGION, // Put your aws region here
  accessKeyId: config.AWS_ACCESS_KEY_ID,
  secretAccessKey: config.AWS_SECRET_KEY,
});

const s3 = new AWS.S3({
  // apiVersion: config.AWS_API_VERSION,
  params: { Bucket: config.AWS_BUCKET_NAME },
});

export const listFiles = () => new Promise((resolve, reject) => {
  const s3params = {};

  s3.listObjects(s3params, (err, data) => {
    if (err) {
      reject(err);
    }

    resolve(data);
  });
});

export const downloadFile = (path, dest) => new Promise((resolve, reject) => {
  const s3Params = { Key: path };
  const writer = fs.createWriteStream(dest);
  const s3File = s3.getObject(s3Params);

  s3File.on('httpData', (chunk) => {
    writer.write(chunk);
  });

  s3File.on('httpDone', () => {
    writer.end();
    resolve(dest);
  });

  s3File.on('error', (err) => {
    reject(err);
  });

  s3File.send();
});

export default { listFiles, downloadFile };
