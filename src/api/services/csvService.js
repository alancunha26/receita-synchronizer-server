import { parse } from 'csv';
import zlib from 'zlib';
import path from 'path';
import fs from 'fs';

/**
 * Instantiate .csv or .csv.gz large files by chunks reader
 *
 * @param {{file: String, chunkSize: Number, skipIndex: Number, formatter: Function}} options
 */
export const createReader = (options) => {
  // Map options
  const {
    file, chunkSize = 1000, skipIndex = 0, formatter,
  } = options;

  // Defines the file basename
  const basename = path.basename(file);

  // Verify if is a valid file
  if (typeof file !== 'string' && fs.statSync(file).isFile()) {
    throw new Error(`${basename} must be a valid file`);
  }

  // Defines the file extension
  const extension = path.extname(file);

  // Verify if the file has the right extension
  if (extension !== '.gz' && extension !== '.csv') {
    throw new Error(`${basename} must be a .gz or .csv file`);
  }

  // Initialize reader object
  const csvReader = {};

  // Initialize chunk data
  csvReader.data = [];

  // Initialize line count
  csvReader.count = 0;

  // Initialize the callbacks
  csvReader.on = (state, callback) => {
    switch (state) {
      case 'chunk':
        csvReader.onChunkCallback = callback;
        break;
      case 'error':
        csvReader.onErrorCallback = callback;
        break;
      case 'end':
        csvReader.onEndCallback = callback;
        break;
      default:
        console.log(`the presented state: "${state}" not exists`);
        break;
    }
  };

  // Starts csv file reader
  csvReader.start = () => {
    const reader = fs
      .createReadStream(file)
      .pipe(zlib.createGunzip())
      .pipe(parse());

    // Resume method to continue iterating over file
    csvReader.continue = () => {
      csvReader.data = [];
      reader.resume();
    };

    reader.on('data', (row) => {
      if (csvReader.count > skipIndex) {
        // Defines the current line
        let currentLine = row;
        // If formatter is defined format line
        if (formatter && typeof formatter === 'function') {
          currentLine = formatter(currentLine);
        }
        // Push data into self state
        csvReader.data.push(currentLine);
      }

      // If chunkSize is reached pause
      if (csvReader.count % chunkSize === 0) {
        reader.pause();
        if (typeof csvReader.onChunkCallback === 'function') {
          csvReader.onChunkCallback(csvReader.data);
        }
      }

      csvReader.count += 1;
    });

    reader.on('end', () => {
      if (typeof csvReader.onEndCallback === 'function') {
        csvReader.onEndCallback();
      }
    });

    reader.on('error', (err) => {
      if (typeof csvReader.onErrorCallback === 'function') {
        csvReader.onErrorCallback(err);
      }
    });
  };

  return csvReader;
};

export default { createReader };
