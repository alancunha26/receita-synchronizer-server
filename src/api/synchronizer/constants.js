import path from 'path';

/**
 * Default directory of files
 */
export const FILES_PATH = path.join(__dirname, 'files');

/**
 * Company kind
 */
export const COMPANY_FILE = 'empresa.csv.gz';
export const COMPANY_KIND = 'COMPANY';

/**
 * Partner kind
 */
export const PARTNER_FILE = 'socio.csv.gz';
export const PARTNER_KIND = 'PARTNER';

/**
 * Cnae kind
 */
export const CNAE_FILE = 'empresa-cnae.csv.gz';
export const CNAE_KIND = 'CNAE';

export default {
  FILES_PATH,
  COMPANY_KIND,
  PARTNER_KIND,
  CNAE_KIND,
};
