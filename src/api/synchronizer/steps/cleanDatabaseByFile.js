import { File } from '../../../db';
import getFileKind from '../utils/getFileKind';
import getFileModel from '../utils/getFileModel';

const cleanDatabaseByFile = async (file) => {
  // Defines the current file kind
  const kind = getFileKind(file);

  // Defines the current file model
  const Model = getFileModel(file);

  // Find the last integrated file document on database
  const lastFile = await File.findOne({ kind }, {}, { sort: { sourceDate: -1 } });

  // If not find an last file return
  if (!lastFile) {
    return;
  }

  // Delte all occurrences from file
  await Model.deleteMany({ fileID: lastFile._id });
  console.log(`Deleted database rows of ${file} from migration: ${lastFile.sourceDate}`);
};

export default cleanDatabaseByFile;
