import fs from 'fs';
import _ from 'lodash';
import path from 'path';
import moment from 'moment';
import { FILES_PATH } from '../constants';
import awsService from '../../services/awsService';

const chooseFiles = async (options) => {
  // Defines the extension
  const extension = options.extension || '.gz';

  // Defines the date format of AWS S3
  const dateFormat = options.dateFormat || 'YYYY-MM-DD';

  // Initializes the last date variable
  let lastDate = moment();

  // Initializes te dates array
  let dates = [lastDate];

  // Initializes the files array
  let files = fs.readdirSync(FILES_PATH).map(file => path.join(FILES_PATH, file));

  // Verify if should download files to list it
  if (options.shouldDownload) {
    // List the objects on AWS S3
    const bucket = await awsService.listFiles();

    // Map bucket files
    const bucketFiles = bucket.Contents.map(bucketFile => bucketFile.Key);

    // Separates what is file and what is a directory
    [files, dates] = _.partition(bucketFiles, file => path.extname(file) === extension);

    // Set the current dir of date
    lastDate = dates.reduce((prevDate, currentDate) => {
      let nextDate = moment(currentDate, dateFormat);
      if (prevDate !== null && !nextDate.isSameOrAfter(prevDate)) {
        nextDate = prevDate;
      }
      return nextDate;
    }, null);

    // Decides what files should be, by date
    files = files.reduce((prevFiles, currentFile) => {
      const nextFiles = prevFiles;
      if (currentFile.includes(lastDate.format(dateFormat))) {
        nextFiles.push(currentFile);
      }
      return nextFiles;
    }, []);
  }

  const info = {
    sourceDate: lastDate.toDate(),
  };

  return [files, info];
};

export default chooseFiles;
