import fs from 'fs';

export const cleanCacheByFile = (file, options) => new Promise((resolve, reject) => {
  console.log(options);

  fs.unlink(file, (err) => {
    if (err) reject(err);
    resolve();
  });
});

export default cleanCacheByFile;
