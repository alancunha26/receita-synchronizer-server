import path from 'path';
import { File } from '../../../db';
import { getFormatter } from '../formatters';
import getFileKind from '../utils/getFileKind';
import getFileModel from '../utils/getFileModel';
import csvService from '../../services/csvService';

/**
 * Read and save a .csv.gz file into database
 *
 * @param {String} file
 * @param {{ sourceDate: Date }} options
 */
const migrateDatabaseByFile = (file, { sourceDate }) => new Promise((resolve, reject) => {
  // Local state variable that tell if already finished to read the file
  let finished = false;

  // Defines file kind
  const kind = getFileKind(file);

  // Model based on kind
  const Model = getFileModel(file);

  // Defines the file name
  const name = path.basename(file);

  // Defines the file formatter
  const formatter = getFormatter(kind);

  // Creates the File document of current migration
  const fileDocument = new File({
    sourceDate,
    name,
    kind,
  });

  // Create chunk reader
  const reader = csvService.createReader({
    formatter,
    file,
  });

  reader.on('chunk', (chunk) => {
    // Add fileID on chunk rows
    const data = chunk.map(row => ({ ...row, fileID: fileDocument._id }));

    // Insert current chunk on database
    Model.insertMany(data, (err, docs) => {
      // Deals error
      if (err) reject(err);

      // If finished reading file save the last migration file on database
      if (finished) {
        fileDocument.save().then(() => {
          console.log(`All data from ${file} was currently saved on database`);
          resolve();
        });
      }

      // continue to next chunk
      console.log(`writed a chunk on database with ${docs.length} lines at row ${reader.count}`);
      reader.continue();
    });

    // If finished reading file save the last migration file on database
    if (finished) {
      fileDocument.save().then(() => {
        console.log(`All data from ${file} was currently saved on database`);
        resolve();
      });
    }
  });

  reader.on('error', (err) => {
    reject(err);
  });

  reader.on('end', () => {
    console.log(`Finished writing on database with ${file}`);
    finished = true;
  });

  reader.start();
});

export default migrateDatabaseByFile;
