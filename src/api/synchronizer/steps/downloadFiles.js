import path from 'path';
import { FILES_PATH } from '../constants';
import awsService from '../../services/awsService';

const createDownloader = dir => async () => {
  // Defines the absolut path of the file
  const destDir = path.join(FILES_PATH, path.basename(dir));

  // Download the file from aws
  const file = await awsService.downloadFile(dir, destDir);

  // Outputs file
  return file;
};

export const downloadFile = async file => createDownloader(file)();

export const downloadFiles = async filesDir => Promise.all(filesDir.map(createDownloader));

export default downloadFiles;
