/* eslint-disable no-continue */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-restricted-syntax */

import _ from 'lodash';
import getFileKind from './utils/getFileKind';
import chooseFiles from './steps/chooseFiles';
import cleanCacheByFile from './steps/cleanCacheByFile';
import cleanDatabaseByFile from './steps/cleanDatabaseByFile';
import migrateDatabaseByFile from './steps/migrateDatabaseByFile';
import downloadFiles, { downloadFile } from './steps/downloadFiles';

const DEFAULT_OPTIONS = {
  /**
   * Defines if should write info on database
   */
  shouldWriteDatabase: true,
  /**
   * Defines if should download files other migrations after migrate
   */
  shouldDownloadFiles: false,
  /**
   * Defines if should clean database other migrations after migrate
   */
  shouldCleanDatabase: false,
  /**
   * Defines if should migrate files concurrenty
   */
  shouldBeConcurrently: false,
  /**
   * Defines if should delete files after migrate
   */
  shouldDeleteFiles: false,
};

export const initialize = async (options = DEFAULT_OPTIONS) => {
  // Merge default options with other options
  options = _.merge(DEFAULT_OPTIONS, options);

  /**
   * Step 1 - Decides what files should download or iterate by order
   */
  const [filesDir, filesInfo] = await chooseFiles(options);

  // Write sourceDate on options object
  options.sourceDate = filesInfo.sourceDate;

  // Breaks the initialization if there are no files
  if (filesDir.length === 0) {
    throw new Error('No files found on amazon s3 or local');
  }

  // Defines the default files with filesDir
  let files = filesDir;

  /**
   * Verify if migration should be concurrently
   */
  if (options.shouldBeConcurrently) {
    const createMigratorByFile = file => async () => {
      /**
       * Step 2 - Download the files from AWS S3
       */
      if (options.shouldDownload) {
        file = await downloadFile(file, options);
      }

      /**
       * Step 3 - Save all files info inside database concurrently
       */
      if (options.shouldWriteDatabase) {
        await migrateDatabaseByFile(file, options);
      }

      /**
       * Step 4 - Clean all database integrations that is not the last migration
       */
      if (options.shouldCleanDatabase) {
        await cleanDatabaseByFile(file, options);
      }

      /**
       * Step 5 - Delete local cache files
       */
      if (options.shouldDeleteFiles) {
        await cleanCacheByFile(file, options);
      }
    };

    // Create a array of Promises that resolve migration
    const promises = files.map(createMigratorByFile);

    // Call concurrently the migration
    await Promise.all(promises);

    // Output the results
    return true;
  }

  /**
   * Step 2 - Download the files from AWS S3
   */
  if (options.shouldDownload) {
    files = await downloadFiles(filesDir);
  }

  // Iterate over all files synchronous
  for (const file of files) {
    // Todo: remove this block of code
    if (getFileKind(file) !== 'COMPANY') {
      continue;
    }

    /**
     * Step 3 - Save all files info inside database concurrently
     */
    if (options.shouldWriteDatabase) {
      await migrateDatabaseByFile(file, options);
    }

    /**
     * Step 4 - Clean all database integrations that is not the last migration
     */
    if (options.shouldCleanDatabase) {
      await cleanDatabaseByFile(file, options);
    }

    /**
     * Step 5 - Delete local cache files
     */
    if (options.shouldDeleteFiles) {
      await cleanCacheByFile(file, options);
    }
  }

  return true;
};

export default { initialize };
