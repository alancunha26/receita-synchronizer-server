import getFileKind from './getFileKind';
import { Company, Cnae } from '../../../db';
import { CNAE_KIND, PARTNER_KIND } from '../constants';

const getFileModel = (file) => {
  if (!file || typeof file !== 'string') {
    throw new Error('file is not defined or not typeof string');
  }

  const kind = getFileKind(file);

  if (kind === CNAE_KIND) {
    return Cnae;
  }

  if (kind === PARTNER_KIND) {
    return Company;
  }

  return Company;
};

export default getFileModel;
