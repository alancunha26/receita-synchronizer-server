import path from 'path';
import {
  COMPANY_KIND,
  COMPANY_FILE,
  CNAE_KIND,
  CNAE_FILE,
  PARTNER_FILE,
  PARTNER_KIND,
} from '../constants';

const getFileKind = (file) => {
  switch (path.basename(file)) {
    case COMPANY_FILE:
      return COMPANY_KIND;
    case CNAE_FILE:
      return CNAE_KIND;
    case PARTNER_FILE:
      return PARTNER_KIND;
    default:
      return COMPANY_KIND;
  }
};

export default getFileKind;
