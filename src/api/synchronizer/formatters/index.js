import formatCnae from './formatCnae';
import formatCompany from './formatCompany';
import formatPartner from './formatPartner';
import { CNAE_KIND, PARTNER_KIND, COMPANY_KIND } from '../constants';

/**
 *
 * @param {String} kind
 */
export const getFormatter = (kind) => {
  switch (kind) {
    case CNAE_KIND:
      return formatCnae;
    case PARTNER_KIND:
      return formatPartner;
    case COMPANY_KIND:
      return formatCompany;
    default:
      return formatCompany;
  }
};

export default {
  getFormatter,
  formatCompany,
  formatPartner,
  formatCnae,
};
