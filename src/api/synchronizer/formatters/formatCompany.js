import _ from 'lodash';
import moment from 'moment';

const formatCompany = (row) => {
  // Phones mapped values
  const phones = _.remove([row[24], row[24]]).map(phone => ({
    number: phone,
  }));

  // Address mapped values
  const address = _.omitBy(
    _.zipObject(
      [
        'logradouro', // 15
        'street', // 16
        'number', // 17
        'neighborhood', // 19
        'zipCode', // 20
        'state', // 21
        'city', // 23
      ],
      _.pullAt(row, [15, 16, 17, 19, 20, 21, 23]),
    ),
    _.isEmpty,
  );

  // Data mapped values
  const data = _.zipObject(
    [
      'cnpj', // 2
      'businessName', // 4
      'fantasyName', // 5
      'cadastralSituation', // 6
      'cadastralSituationDate', // 7
      'cadastralSituationReason', // 8
      'legalNature', // 12
      'startActivityDate', // 13
      'primaryCnae', // 14
      'email', // 27
    ],
    _.pullAt(row, [2, 4, 5, 6, 7, 8, 12, 13, 14, 27]),
  );

  // Convert dates to javascript Dates
  const dataMap = _.mapValues({ ...data, address, phones }, (value, key) => {
    if (key === 'startActivityDate' || key === 'cadastralSituationDate') {
      const date = moment(value, 'YYYY-MM-DD');

      if (!date.isValid()) {
        return null;
      }

      return date.toDate();
    }

    return value;
  });

  return _.pickBy(dataMap, _.identity);
};

export default formatCompany;
