import authModule from './auth';
import companyModule from './company';

const modules = [authModule, companyModule];

export default modules;
