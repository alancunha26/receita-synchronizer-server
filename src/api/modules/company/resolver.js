import { Company } from '../../../db';

export const getCompanyByID = async (req, res) => {
  const { id } = req.params;

  try {
    // Query data on datbase
    const data = await Company.findById(id);

    // Send data
    res.status(200).send({
      data,
    });
  } catch (err) {
    res.status(400).send({
      error: err.message,
    });
  }
};

export const listCompaniesByPage = async (req, res) => {
  // Map params
  const { page = 0 } = req.params;

  try {
    // Query data on datbase
    const data = await Company.find()
      .skip(page * 15)
      .limit(15);

    // Send data
    res.status(200).send({
      data,
    });
  } catch (err) {
    res.status(400).send({
      error: err.message,
    });
  }
};
