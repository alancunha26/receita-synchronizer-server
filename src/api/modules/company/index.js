import * as companySchema from './schema';
import * as companyResolver from './resolver';

const companyModule = {
  name: 'company',

  router: [
    {
      method: 'get',
      path: '/:id',

      resolver: companyResolver.getCompanyByID,
      schema: companySchema.getCompanyByID,
    },
    {
      method: 'get',
      path: '/list/:page',

      resolver: companyResolver.listCompaniesByPage,
      schema: companySchema.listCompaniesByPage,
    },
  ],
};

export default companyModule;
