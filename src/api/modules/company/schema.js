import Joi from 'joi';

export const getCompanyByID = {
  params: Joi.object().keys({
    id: Joi.string().required(),
  }),
};

export const listCompaniesByPage = {
  params: Joi.object().keys({
    page: Joi.string(),
  }),
};
