import Joi from 'joi';

export const signin = {
  body: Joi.object().keys({
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .min(6)
      .required(),
  }),
};

export const signup = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .min(6)
      .required(),
  }),
};

export const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string()
      .email()
      .required(),
  }),
};
