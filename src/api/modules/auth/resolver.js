import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import db from '../../../db';

export const signup = async (req, res) => {
  try {
    const { name, email, password } = req.body;

    let user = await db.models.User.findOne({ where: { email } });

    if (user) {
      throw new Error('User already exists');
    }

    const digest = await bcrypt.hash(password, 10);
    user = await db.models.User.create({ digest, email, name });

    const token = jwt.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET);

    res.status(200).send({ token, user });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

export const signin = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await db.models.User.findOne({ where: { email } });

    if (!user) {
      throw new Error('Invalid email or password');
    }

    const valid = await bcrypt.compare(password, user.digest);

    if (!valid) {
      throw new Error('Invalid email or password');
    }

    const token = jwt.sign({ id: user.id, email: user.email }, process.env.JWT_SECRET);

    res.status(200).send({ token, user });
  } catch (error) {
    res.status(200).send({ error: error.message });
  }
};

export const forgotPassword = async (req, res) => {
  res.status(200).send({
    ok: true,
  });
};
