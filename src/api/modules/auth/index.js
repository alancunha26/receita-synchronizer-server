import * as authSchema from './schema';
import * as authResolver from './resolver';

const authModule = {
  name: 'auth',

  router: [
    {
      method: 'post',
      path: '/signup',

      resolver: authResolver.signup,
      schema: authSchema.signup,
    },
    {
      method: 'post',
      path: '/signin',

      resolver: authResolver.signin,
      schema: authSchema.signin,
    },
    {
      method: 'post',
      path: '/forgot-password',

      resolver: authResolver.forgotPassword,
      schema: authSchema.forgotPassword,
    },
  ],
};

export default authModule;
