import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import middlewares from './middlewares';
import framework from './framework';
import modules from './modules';

const app = express();
const apiPath = process.env.API_PATH;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/static', express.static(path.join(__dirname, 'public')));

framework.initialize({
  app, middlewares, modules, apiPath,
});

export default app;
