import dotenv from 'dotenv';

if (process.env.NODE_ENV !== 'production') {
  const env = dotenv.config();

  if (env.error) {
    throw env.error;
  }
}
